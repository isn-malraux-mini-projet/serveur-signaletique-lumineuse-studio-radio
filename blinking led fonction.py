import RPi.GPIO as GPIO
import time

LedPin = 40    # pin 40

def setup():
  GPIO.setmode(GPIO.BOARD)       # met le gpio avec son emplacement physiquement
  GPIO.setup(LedPin, GPIO.OUT)   # met le pin 40 en output
  GPIO.output(LedPin, GPIO.HIGH) # met pin 40 en hight (+3,3V)

def blink():
  while True:
    GPIO.output(LedPin, GPIO.HIGH)  # led allumée
    time.sleep(1)
    GPIO.output(LedPin, GPIO.LOW) # led éteinte
    time.sleep(1)

if __name__ == '__main__':     # début de la boucle
  setup()
  blink()