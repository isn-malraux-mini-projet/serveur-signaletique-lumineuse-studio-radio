from threading import Thread 
import RPi.GPIO as GPIO
import time
import liblo

LedPin = 40    # pin 39

    
def blink():
    def setup():
      GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
      GPIO.setup(LedPin, GPIO.OUT)   # Set LedPin's mode is output
      GPIO.output(LedPin, GPIO.HIGH) # Set LedPin high(+3.3V) to turn on led
    
    def blink():
      while True:
        GPIO.output(LedPin, GPIO.HIGH)  # led on
        time.sleep(1)
        GPIO.output(LedPin, GPIO.LOW) # led off
        time.sleep(1)
    
    if __name__ == '__main__':     # beginning of the program
      setup()
      blink()

def serveur():
    def interrupteur_callback(chemin, parametres):
        global INTERRUPTEUR
        clef_securite = parametres[0]
        etat_demande = parametres[1]
        print("Demande reçu pour mettre l'interrupteur sur '{}'".format(etat_demande))
        if clef_securite == "GOOD":
            print("Clef de sécurité '{}' vérifiée, demande acceptée".format(clef_securite))
            ancien_etat = INTERRUPTEUR
            INTERRUPTEUR = etat_demande
            print("Interrupteur positionné depuis '{}' vers '{}'".format(ancien_etat, etat_demande))
        else:
            print("Clef de sécurité '{}' invalide, demande refusée".format(clef_securite))
            print("Interrupteur conservé en l'état '{}'".format(INTERRUPTEUR))
    
    def capture_tout_le_reste_callback(chemin, parametres, types, origine):
        print("Réception d'un message OSC provenant de '{}' et demandant '{}'".format(origine.url, chemin))
        for valeur_parametre, type in zip(parametres, types):
            print("Paramètre de type '{}' = {}".format(type, valeur_parametre))
    
    serveur = liblo.Server(port=1234)
    serveur.add_method("/signaletique/enregistrement/interrupteur", "sT", interrupteur_callback)
    serveur.add_method("/signaletique/enregistrement/interrupteur", "sF", interrupteur_callback)
    serveur.add_method(None, None, capture_tout_le_reste_callback)


# Création des threads

thread_1 = blink()

thread_2 = serveur()


# Lancement des threads

thread_1.start()

thread_2.start()


# Attend que les threads se terminent

thread_1.join()

thread_2.join()






"""SERVER THREAD TEST#2"""


import liblo
from threading import Thread
import RPi.GPIO as GPIO
import time

serveur = None
port = 1234
class Serv(Thread):
        def __init__(self):
            Thread.__init__(self)
            print("Thread initialisé ...")
        def run(self):
            global serveur, port
            serveur = liblo.Server(port)
            print("Le serveur a été créé")
            print("Le serveur écoute sur le port '{0}' ".format(port))
            while True:
                serveur.recv(50)
                serveur.add_method("/signaletique/enregistrement/interrupteur", "sT", interrupteur_callback)
                serveur.add_method("/signaletique/enregistrement/interrupteur", "sF", interrupteur_callback)
                serveur.add_method(None, None, capture_tout_le_reste_callback)
thread = Serv()
thread.start()

INTERRUPTEUR = False

def interrupteur_callback(chemin, parametres):
    global INTERRUPTEUR
    clef_securite = parametres[0]
    etat_demande = parametres[1]
    print("Demande reçu pour mettre l'interrupteur sur '{}'".format(etat_demande))
    if clef_securite == "GOOD":
        print("Clef de sécurité '{}' vérifiée, demande acceptée".format(clef_securite))
        ancien_etat = INTERRUPTEUR
        INTERRUPTEUR = etat_demande
        print("Interrupteur positionné depuis '{}' vers '{}'".format(ancien_etat, etat_demande))
        Allumer_les_Led()
        print("Les Leds sont allumées !")
    else:
        print("Clef de sécurité '{}' invalide, demande refusée".format(clef_securite))
        print("Interrupteur conservé en l'état '{}'".format(INTERRUPTEUR))

def capture_tout_le_reste_callback(chemin, parametres, types, origine):
    print("Réception d'un message OSC provenant de '{}' et demandant '{}'".format(origine.url, chemin))
    for valeur_parametre, type in zip(parametres, types):
        print("Paramètre de type '{}' = {}".format(type, valeur_parametre))
    interrupteur_callback(chemin, parametres)    

LedPin = 40    # pin 40

def initialisation():
  GPIO.setmode(GPIO.BOARD)       # met le gpio avec son emplacement physiquement
  GPIO.setup(LedPin, GPIO.OUT)   # met le pin 40 en output
  GPIO.output(LedPin, GPIO.HIGH) # met pin 40 en hight (+3,3V)

initialisation()

def  Faire_Clignoter_les_Led():
  while True:
    GPIO.output(LedPin, GPIO.HIGH)  # led allumée
    time.sleep(1)
    GPIO.output(LedPin, GPIO.LOW) # led éteinte
    time.sleep(1)

def  Allumer_les_Led():
    GPIO.output(LedPin, GPIO.HIGH)  # led allumée

def Eteindre_les_leds():
    GPIO.output(LedPin, GPIO.LOW)  # led éteinte

if __name__ == '__main__':     # début de la boucle
  initialisation()
  Faire_Clignoter_Les_Led()
